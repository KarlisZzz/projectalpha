//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IKS.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Concerts
    {
        public System.Guid ConcertId { get; set; }
        public string ConcertName { get; set; }
        public string ConcertDescription { get; set; }
        public Nullable<System.Guid> ConcertDate { get; set; }
        public Nullable<short> StatusId { get; set; }
    }
}
