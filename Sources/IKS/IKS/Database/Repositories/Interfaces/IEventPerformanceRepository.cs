﻿using IKS.Models;
using System;
using System.Collections.Generic;

namespace IKS.Database.Repositories
{
    public interface IEventPerformanceRepository
    {
        void AddPerformance(PerformanceModel model);
        void AddPerformances(Guid eventId, List<Guid> performanceIds);
        void Delete(Guid performanceId, Guid eventId);
    }
}