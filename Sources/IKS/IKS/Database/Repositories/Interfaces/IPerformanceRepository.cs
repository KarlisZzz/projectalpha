﻿using System;
using System.Collections.Generic;

namespace IKS.Database.Repositories
{
    public interface IPerformanceRepository : IBaseRepository<Performances>
    {
        List<Performances> GetPerformancesByEventId(Guid eventId);
        List<Performances> GetPerformancesNotIncludedinEventId(Guid eventId);
        Performances Save(Performances entity);

    }
}