﻿using System;
using System.Collections.Generic;

namespace IKS.Database.Repositories
{
    public interface IItemRepository : IBaseRepository<Items>
    {
        List<AvailableItems> GetAllAvailableItems();
        List<Items> GetItemsByUserId(Guid userId);
        List<Items> GetItemsByParentId(Guid parentId);
        Items Save(Items entity);
    }
}