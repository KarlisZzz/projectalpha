﻿using System;
using System.Collections.Generic;

namespace IKS.Database.Repositories
{
    public interface IUserRepository : IBaseRepository<Users>
    {
        List<Users> GetUsersByItemId(Guid itemId);
        Users Save(Users entity);
    }
}