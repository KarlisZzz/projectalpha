﻿using System;
using System.Collections.Generic;

namespace IKS.Database.Repositories
{
    public interface IUserItemRepository
    {
        void AddItems(Guid userId, List<Guid> itemIds);
        void Delete(Guid userId, Guid itemId);
        void ReturnItems(Guid userId, List<Guid> itemIds);
    }
}