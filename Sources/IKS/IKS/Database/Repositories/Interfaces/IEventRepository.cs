﻿using System;
using System.Collections.Generic;

namespace IKS.Database.Repositories
{
    public interface IEventRepository : IBaseRepository<Events>
    {
        Events Save(Events entity);
        List<Events> GetEventsByPerformanceId(Guid performanceId);
    }
}