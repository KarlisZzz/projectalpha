﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Database.Repositories
{
    public class PerformanceRepository : BaseRepository<Performances>, IPerformanceRepository
    {
        public PerformanceRepository(IKSDatabaseEntities context) : base(context)
        {
        }
        public List<Performances> GetPerformancesByEventId(Guid eventId)
        {
            return context.EventsPerformances.Where(ui => ui.EventId == eventId).Select(ui => ui.Performances).ToList();
        }
        public List<Performances> GetPerformancesNotIncludedinEventId(Guid eventId)
        {
            var existingPerformances = context.EventsPerformances.Where(ui => ui.EventId == eventId).Select(ui => ui.PerformanceId);
            return context.Performances.Where(ui=> !existingPerformances.Contains(ui.PerformanceId)).ToList();
        }
        public Performances Save(Performances entity)
        {
            var dbEntity = context.Performances.FirstOrDefault(p => p.PerformanceId == entity.PerformanceId);
            if (dbEntity == null)
                context.Performances.Add(entity);
            else
                context.Entry(dbEntity).CurrentValues.SetValues(entity);

            context.SaveChanges();
            return context.Performances.FirstOrDefault(p => p.PerformanceId == entity.PerformanceId);

        }
    }
}