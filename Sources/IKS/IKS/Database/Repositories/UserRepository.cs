﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Database.Repositories
{
    public class UserRepository : BaseRepository<Users>, IUserRepository
    {
        public UserRepository(IKSDatabaseEntities context) : base(context)
        {
        }
        public List<Users> GetUsersByItemId(Guid itemId)
        {
            return context.UsersItems.Where(ui => ui.ItemId == itemId).Select(ui => ui.Users).ToList();
        }
        public Users Save(Users entity)
        {
            var dbEntity = context.Users.FirstOrDefault(p => p.UserId == entity.UserId);
            if (dbEntity == null)
                context.Users.Add(entity);
            else
                context.Entry(dbEntity).CurrentValues.SetValues(entity);

            context.SaveChanges();
            return context.Users.FirstOrDefault(p => p.UserId == entity.UserId);

        }
    }
}