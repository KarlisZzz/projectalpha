﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IKS.Database.Repositories
{
    public class EventRepository : BaseRepository<Events>, IEventRepository
    {
        public EventRepository(IKSDatabaseEntities context) : base(context)
        {
        }
        public Events Save(Events entity)
        {
            var dbEntity = context.Events.FirstOrDefault(p => p.EventId == entity.EventId);
            if (dbEntity == null)
                context.Events.Add(entity);
            else
            {
                context.Entry(dbEntity).CurrentValues.SetValues(entity);
                context.Entry(dbEntity).State = EntityState.Modified;
            }
                
            context.SaveChanges();
            return context.Events.FirstOrDefault(p => p.EventId == entity.EventId);

        }
        public List<Events> GetEventsByPerformanceId(Guid performanceId)
        {
            return context.EventsPerformances.Where(ep => ep.PerformanceId == performanceId).Select(ep => ep.Events).ToList();
        }
    }
}