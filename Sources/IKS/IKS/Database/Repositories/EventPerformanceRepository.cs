﻿using IKS.BusinessLogic.Extensions;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Database.Repositories
{
    public class EventPerformanceRepository : BaseRepository<EventsPerformances>, IEventPerformanceRepository
    {
        public EventPerformanceRepository(IKSDatabaseEntities context) : base(context)
        {
        }
        public void AddPerformance(PerformanceModel model)
        {
            if (model.EventId.HasValue)
            {
                if (!context.EventsPerformances.Any(ep => ep.EventId == model.EventId && ep.PerformanceId == model.PerformanceId))
                {
                    var entity = Mapper<PerformanceModel, EventsPerformances>.Map(model);
                    entity.PerformanceId = model.PerformanceId;
                    entity.EventPerformanceId = Guid.NewGuid();
                    context.EventsPerformances.Add(entity);
                    context.SaveChanges();
                }
            }
        }
        public void AddPerformances(Guid eventId, List<Guid> performanceIds)
        {
            var existingPerformances = context.EventsPerformances.Where(ep => ep.EventId == eventId && performanceIds.Contains(ep.PerformanceId.Value)).Select(ep=>ep.PerformanceId);
            foreach (Guid pId in performanceIds.Where(ep => !existingPerformances.Contains(ep)))
            {
                var entity = new EventsPerformances()
                {
                    EventPerformanceId = Guid.NewGuid(),
                    EventId = eventId,
                    PerformanceId = pId
                };
                context.EventsPerformances.Add(entity);
            }
            context.SaveChanges();
        }
        public void Delete(Guid performanceId, Guid eventId)
        {
            var entity = context.EventsPerformances.FirstOrDefault(ep => ep.EventId == eventId && ep.PerformanceId == performanceId);
            if (entity != null)
            {
                context.EventsPerformances.Remove(entity);
                context.SaveChanges();
            }
        }
    }
}