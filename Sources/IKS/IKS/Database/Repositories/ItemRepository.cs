﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Database.Repositories
{
    public class ItemRepository : BaseRepository<Items>, IItemRepository
    {
        public ItemRepository(IKSDatabaseEntities context) : base(context)
        {
        }
        public List<AvailableItems> GetAllAvailableItems()
        {
            return context.AvailableItems.ToList();
        }

        public List<Items> GetItemsByUserId(Guid userId)
        {
            return context.UsersItems.Where(ui => ui.UserId == userId && !ui.RecievedDate.HasValue).Select(ui => ui.Items).ToList();
        }
        public List<Items> GetItemsByParentId(Guid parentId)
        {
            return context.Items.Where(ui => ui.ParentId == parentId).ToList();
        }
        public Items Save(Items entity)
        {
            var dbEntity = context.Items.FirstOrDefault(p => p.ItemId == entity.ItemId);
            if (dbEntity == null)
                context.Items.Add(entity);
            else
                context.Entry(dbEntity).CurrentValues.SetValues(entity);

            context.SaveChanges();
            return context.Items.FirstOrDefault(p => p.ItemId == entity.ItemId);

        }
    }
}