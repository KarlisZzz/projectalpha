﻿using IKS.BusinessLogic.Extensions;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Database.Repositories
{
    public class UserItemRepository : BaseRepository<UsersItems>, IUserItemRepository
    {
        public UserItemRepository(IKSDatabaseEntities context) : base(context)
        {
        }
        private IQueryable<UsersItems> ExistingItems => context.UsersItems.Where(ep => !ep.RecievedDate.HasValue);
        public void AddItems(Guid userId, List<Guid> itemIds)
        {
            var existingItems = ExistingItems.Where(ep => ep.UserId == userId && itemIds.Contains(ep.ItemId.Value)).Select(ep => ep.ItemId);
            foreach (Guid iId in itemIds.Where(ep => !existingItems.Contains(ep)))
            {
                var entity = new UsersItems()
                {
                    UsersItemsId = Guid.NewGuid(),
                    UserId = userId,
                    ItemId = iId,
                    HandoverDate = DateTime.Today
                };
                context.UsersItems.Add(entity);
            }
            context.SaveChanges();
        }
        public void ReturnItems(Guid userId, List<Guid> itemIds)
        {
            var existingItems = ExistingItems.Where(ep => ep.UserId == userId && itemIds.Contains(ep.ItemId.Value));
            foreach (var item in existingItems)
            {
                item.RecievedDate = DateTime.Today;
            }
            context.SaveChanges();
        }
        public void Delete(Guid userId, Guid itemId)
        {
            var entity = context.UsersItems.FirstOrDefault(ep => ep.UserId == userId && ep.ItemId == itemId);
            if (entity != null)
            {
                context.UsersItems.Remove(entity);
                context.SaveChanges();
            }
        }
    }
}