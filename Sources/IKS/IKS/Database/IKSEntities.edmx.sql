
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/04/2019 11:50:02
-- Generated from EDMX file: C:\Projects\IKS\projectalpha\Sources\IKS\IKS\Database\IKSEntities.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [IKSDatabase];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_EventsPerformances_Events]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EventsPerformances] DROP CONSTRAINT [FK_EventsPerformances_Events];
GO
IF OBJECT_ID(N'[dbo].[FK_EventsPerformances_Performances]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EventsPerformances] DROP CONSTRAINT [FK_EventsPerformances_Performances];
GO
IF OBJECT_ID(N'[dbo].[FK_Items_ItemTypes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Items] DROP CONSTRAINT [FK_Items_ItemTypes];
GO
IF OBJECT_ID(N'[dbo].[FK_ItemsLocationHistory_Items]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ItemsLocationHistory] DROP CONSTRAINT [FK_ItemsLocationHistory_Items];
GO
IF OBJECT_ID(N'[dbo].[FK_ItemsLocationHistory_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ItemsLocationHistory] DROP CONSTRAINT [FK_ItemsLocationHistory_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_PerformancesItems_Events]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PerformancesItems] DROP CONSTRAINT [FK_PerformancesItems_Events];
GO
IF OBJECT_ID(N'[dbo].[FK_PerformancesItems_Performances]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PerformancesItems] DROP CONSTRAINT [FK_PerformancesItems_Performances];
GO
IF OBJECT_ID(N'[dbo].[FK_PerformancesUsers_Events]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PerformancesUsers] DROP CONSTRAINT [FK_PerformancesUsers_Events];
GO
IF OBJECT_ID(N'[dbo].[FK_PerformancesUsers_Performances]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PerformancesUsers] DROP CONSTRAINT [FK_PerformancesUsers_Performances];
GO
IF OBJECT_ID(N'[dbo].[FK_UsersItems_Events]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UsersItems] DROP CONSTRAINT [FK_UsersItems_Events];
GO
IF OBJECT_ID(N'[dbo].[FK_UsersItems_Performances]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UsersItems] DROP CONSTRAINT [FK_UsersItems_Performances];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Events]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Events];
GO
IF OBJECT_ID(N'[dbo].[EventsPerformances]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EventsPerformances];
GO
IF OBJECT_ID(N'[dbo].[Items]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Items];
GO
IF OBJECT_ID(N'[dbo].[ItemsLocationHistory]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ItemsLocationHistory];
GO
IF OBJECT_ID(N'[dbo].[ItemTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ItemTypes];
GO
IF OBJECT_ID(N'[dbo].[Performances]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Performances];
GO
IF OBJECT_ID(N'[dbo].[PerformancesItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PerformancesItems];
GO
IF OBJECT_ID(N'[dbo].[PerformancesUsers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PerformancesUsers];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[UsersItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UsersItems];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Items'
CREATE TABLE [dbo].[Items] (
    [ItemId] uniqueidentifier  NOT NULL,
    [ItemTypeId] uniqueidentifier  NULL,
    [Name] nvarchar(50)  NULL,
    [Code] nvarchar(20)  NULL,
    [Description] nvarchar(20)  NULL,
    [UserAssignedTo] uniqueidentifier  NULL,
    [StatusId] smallint  NULL
);
GO

-- Creating table 'ItemsLocationHistory'
CREATE TABLE [dbo].[ItemsLocationHistory] (
    [ItemsLocationHistoryId] uniqueidentifier  NOT NULL,
    [ItemId] uniqueidentifier  NULL,
    [UserId] uniqueidentifier  NULL,
    [AssignedDate] datetime  NULL,
    [ReceivedDate] datetime  NULL
);
GO

-- Creating table 'ItemTypes'
CREATE TABLE [dbo].[ItemTypes] (
    [ItemTypeId] uniqueidentifier  NOT NULL,
    [Name] nvarchar(20)  NULL,
    [Code] nvarchar(20)  NULL,
    [Description] nvarchar(500)  NULL,
    [GenderId] smallint  NULL,
    [StatusId] smallint  NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserId] uniqueidentifier  NOT NULL,
    [Login] nvarchar(50)  NULL,
    [UserName] nvarchar(20)  NULL,
    [Surname] nvarchar(20)  NULL,
    [GenderId] smallint  NULL,
    [StatusId] smallint  NULL
);
GO

-- Creating table 'Events'
CREATE TABLE [dbo].[Events] (
    [EventId] uniqueidentifier  NOT NULL,
    [EventName] nvarchar(50)  NULL,
    [EventDescription] nvarchar(500)  NULL,
    [EventDate] uniqueidentifier  NULL,
    [StatusId] smallint  NULL
);
GO

-- Creating table 'EventsPerformances'
CREATE TABLE [dbo].[EventsPerformances] (
    [EventPerformanceId] uniqueidentifier  NOT NULL,
    [EventId] uniqueidentifier  NULL,
    [PerformanceId] uniqueidentifier  NULL
);
GO

-- Creating table 'Performances'
CREATE TABLE [dbo].[Performances] (
    [PerformanceId] uniqueidentifier  NOT NULL,
    [PerformanceName] nvarchar(50)  NULL,
    [Description] nvarchar(500)  NULL,
    [PerformanceTime] uniqueidentifier  NULL,
    [StatusId] smallint  NULL
);
GO

-- Creating table 'PerformancesItems'
CREATE TABLE [dbo].[PerformancesItems] (
    [PerformancesItemsId] uniqueidentifier  NOT NULL,
    [PerformanceId] uniqueidentifier  NULL,
    [ItemId] uniqueidentifier  NULL
);
GO

-- Creating table 'PerformancesUsers'
CREATE TABLE [dbo].[PerformancesUsers] (
    [PerformancesUsersId] uniqueidentifier  NOT NULL,
    [PerformanceId] uniqueidentifier  NULL,
    [UserId] uniqueidentifier  NULL
);
GO

-- Creating table 'UsersItems'
CREATE TABLE [dbo].[UsersItems] (
    [UsersItemsId] uniqueidentifier  NOT NULL,
    [ItemId] uniqueidentifier  NULL,
    [UserId] uniqueidentifier  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ItemId] in table 'Items'
ALTER TABLE [dbo].[Items]
ADD CONSTRAINT [PK_Items]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemsLocationHistoryId] in table 'ItemsLocationHistory'
ALTER TABLE [dbo].[ItemsLocationHistory]
ADD CONSTRAINT [PK_ItemsLocationHistory]
    PRIMARY KEY CLUSTERED ([ItemsLocationHistoryId] ASC);
GO

-- Creating primary key on [ItemTypeId] in table 'ItemTypes'
ALTER TABLE [dbo].[ItemTypes]
ADD CONSTRAINT [PK_ItemTypes]
    PRIMARY KEY CLUSTERED ([ItemTypeId] ASC);
GO

-- Creating primary key on [UserId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [EventId] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [PK_Events]
    PRIMARY KEY CLUSTERED ([EventId] ASC);
GO

-- Creating primary key on [EventPerformanceId] in table 'EventsPerformances'
ALTER TABLE [dbo].[EventsPerformances]
ADD CONSTRAINT [PK_EventsPerformances]
    PRIMARY KEY CLUSTERED ([EventPerformanceId] ASC);
GO

-- Creating primary key on [PerformanceId] in table 'Performances'
ALTER TABLE [dbo].[Performances]
ADD CONSTRAINT [PK_Performances]
    PRIMARY KEY CLUSTERED ([PerformanceId] ASC);
GO

-- Creating primary key on [PerformancesItemsId] in table 'PerformancesItems'
ALTER TABLE [dbo].[PerformancesItems]
ADD CONSTRAINT [PK_PerformancesItems]
    PRIMARY KEY CLUSTERED ([PerformancesItemsId] ASC);
GO

-- Creating primary key on [PerformancesUsersId] in table 'PerformancesUsers'
ALTER TABLE [dbo].[PerformancesUsers]
ADD CONSTRAINT [PK_PerformancesUsers]
    PRIMARY KEY CLUSTERED ([PerformancesUsersId] ASC);
GO

-- Creating primary key on [UsersItemsId] in table 'UsersItems'
ALTER TABLE [dbo].[UsersItems]
ADD CONSTRAINT [PK_UsersItems]
    PRIMARY KEY CLUSTERED ([UsersItemsId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ItemTypeId] in table 'Items'
ALTER TABLE [dbo].[Items]
ADD CONSTRAINT [FK_Items_ItemTypes]
    FOREIGN KEY ([ItemTypeId])
    REFERENCES [dbo].[ItemTypes]
        ([ItemTypeId])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Items_ItemTypes'
CREATE INDEX [IX_FK_Items_ItemTypes]
ON [dbo].[Items]
    ([ItemTypeId]);
GO

-- Creating foreign key on [ItemId] in table 'ItemsLocationHistory'
ALTER TABLE [dbo].[ItemsLocationHistory]
ADD CONSTRAINT [FK_ItemsLocationHistory_Items]
    FOREIGN KEY ([ItemId])
    REFERENCES [dbo].[Items]
        ([ItemId])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ItemsLocationHistory_Items'
CREATE INDEX [IX_FK_ItemsLocationHistory_Items]
ON [dbo].[ItemsLocationHistory]
    ([ItemId]);
GO

-- Creating foreign key on [UserId] in table 'ItemsLocationHistory'
ALTER TABLE [dbo].[ItemsLocationHistory]
ADD CONSTRAINT [FK_ItemsLocationHistory_Users]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ItemsLocationHistory_Users'
CREATE INDEX [IX_FK_ItemsLocationHistory_Users]
ON [dbo].[ItemsLocationHistory]
    ([UserId]);
GO

-- Creating foreign key on [EventId] in table 'EventsPerformances'
ALTER TABLE [dbo].[EventsPerformances]
ADD CONSTRAINT [FK_EventsPerformances_Events]
    FOREIGN KEY ([EventId])
    REFERENCES [dbo].[Events]
        ([EventId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EventsPerformances_Events'
CREATE INDEX [IX_FK_EventsPerformances_Events]
ON [dbo].[EventsPerformances]
    ([EventId]);
GO

-- Creating foreign key on [PerformanceId] in table 'EventsPerformances'
ALTER TABLE [dbo].[EventsPerformances]
ADD CONSTRAINT [FK_EventsPerformances_Performances]
    FOREIGN KEY ([PerformanceId])
    REFERENCES [dbo].[Performances]
        ([PerformanceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EventsPerformances_Performances'
CREATE INDEX [IX_FK_EventsPerformances_Performances]
ON [dbo].[EventsPerformances]
    ([PerformanceId]);
GO

-- Creating foreign key on [ItemId] in table 'PerformancesItems'
ALTER TABLE [dbo].[PerformancesItems]
ADD CONSTRAINT [FK_PerformancesItems_Events]
    FOREIGN KEY ([ItemId])
    REFERENCES [dbo].[Items]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PerformancesItems_Events'
CREATE INDEX [IX_FK_PerformancesItems_Events]
ON [dbo].[PerformancesItems]
    ([ItemId]);
GO

-- Creating foreign key on [ItemId] in table 'UsersItems'
ALTER TABLE [dbo].[UsersItems]
ADD CONSTRAINT [FK_UsersItems_Performances]
    FOREIGN KEY ([ItemId])
    REFERENCES [dbo].[Items]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersItems_Performances'
CREATE INDEX [IX_FK_UsersItems_Performances]
ON [dbo].[UsersItems]
    ([ItemId]);
GO

-- Creating foreign key on [PerformanceId] in table 'PerformancesItems'
ALTER TABLE [dbo].[PerformancesItems]
ADD CONSTRAINT [FK_PerformancesItems_Performances]
    FOREIGN KEY ([PerformanceId])
    REFERENCES [dbo].[Performances]
        ([PerformanceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PerformancesItems_Performances'
CREATE INDEX [IX_FK_PerformancesItems_Performances]
ON [dbo].[PerformancesItems]
    ([PerformanceId]);
GO

-- Creating foreign key on [PerformanceId] in table 'PerformancesUsers'
ALTER TABLE [dbo].[PerformancesUsers]
ADD CONSTRAINT [FK_PerformancesUsers_Performances]
    FOREIGN KEY ([PerformanceId])
    REFERENCES [dbo].[Performances]
        ([PerformanceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PerformancesUsers_Performances'
CREATE INDEX [IX_FK_PerformancesUsers_Performances]
ON [dbo].[PerformancesUsers]
    ([PerformanceId]);
GO

-- Creating foreign key on [UserId] in table 'PerformancesUsers'
ALTER TABLE [dbo].[PerformancesUsers]
ADD CONSTRAINT [FK_PerformancesUsers_Events]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PerformancesUsers_Events'
CREATE INDEX [IX_FK_PerformancesUsers_Events]
ON [dbo].[PerformancesUsers]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'UsersItems'
ALTER TABLE [dbo].[UsersItems]
ADD CONSTRAINT [FK_UsersItems_Events]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersItems_Events'
CREATE INDEX [IX_FK_UsersItems_Events]
ON [dbo].[UsersItems]
    ([UserId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------