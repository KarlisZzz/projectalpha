﻿using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Database
{
    public static class MockDb
    {
        public static List<UserModel> GetAllUsers()
        {
            List<UserModel> userList = new List<UserModel>();
            userList.Add(new UserModel() { UserId = new Guid("4e0cd210-7de4-41eb-8250-2ed2fa973cf7"), Login = "KZS", UserName = "Karlis", Surname = "Ziemulis", Status = UserStatus.Active, GenderId = Genders.Male });
            userList.Add(new UserModel() { UserId = new Guid("78444814-d3f9-4996-a1dd-191a54a0c120"), Login = "SMW", UserName = "Some", Surname = "Other", Status = UserStatus.Active, GenderId = Genders.Female });
            userList.Add(new UserModel() { UserId = new Guid("d62ce28c-1993-4d69-8279-fc465c6b221d"), Login = "AAA", UserName = "A", Surname = "AA", Status = UserStatus.Deactivated, GenderId = Genders.Female });
            return userList;
        }
        public static List<ItemModel> GetAllItems()
        {
            List<ItemModel> userList = new List<ItemModel>();
            userList.Add(new ItemModel() { ItemId = new Guid("21b8295f-d4d6-4309-ba86-aae5f36a0880"), Name = "Svārki", Code = "01", Description = "Svārki description", Status = ItemStatus.Active });
            userList.Add(new ItemModel() { ItemId = new Guid("d11eb353-3bcc-4240-bea9-f17695eafaea"), Name = "Bikses", Code = "02", Description = "Bikses description", Status = ItemStatus.Active });
            userList.Add(new ItemModel() { ItemId = new Guid("0ee95187-2f5b-4839-b62e-ce3a38a75f0e"), Name = "Veste", Code = "03", Description = "Veste description", Status = ItemStatus.Deactivated });
            return userList;
        }

        public static List<ItemTypes> GetAllItemTypes()
        {
            List<ItemTypes> userList = new List<ItemTypes>();
            userList.Add(new ItemTypes() { ItemTypeId = new Guid("21b8295f-d4d6-4309-ba86-aae5f36a0880"), Name = "Svārki", Code = "SVK", Description = "Svārki description", GenderId = 1 });
            userList.Add(new ItemTypes() { ItemTypeId = new Guid("d11eb353-3bcc-4240-bea9-f17695eafaea"), Name = "Bikses", Code = "BKS", Description = "Bikses description", GenderId = 1 });
            userList.Add(new ItemTypes() { ItemTypeId = new Guid("0ee95187-2f5b-4839-b62e-ce3a38a75f0e"), Name = "Veste", Code = "VES", Description = "Veste description", GenderId = 1 });
            return userList;
        }
        public static List<EventModel> GetAllConcerts()
        {
            List<EventModel> userList = new List<EventModel>();
            userList.Add(new EventModel() { EventId = new Guid("21b8295f-d4d6-4309-ba86-aae5f36a0881"), EventName = "Some concert"});
            return userList;
        }
        public static List<PerformanceModel> GetAllPerformances()
        {
            List<PerformanceModel> userList = new List<PerformanceModel>();
            userList.Add(new PerformanceModel() { PerformanceId = new Guid("21b8295f-d4d6-4309-ba86-aae5f36a0882"), PerformanceName = "Some dance" });
            return userList;
        }
        public enum Genders
        {
            Male = 0,
            Female = 1
        }
    }
}