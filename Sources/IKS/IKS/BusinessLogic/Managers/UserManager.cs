﻿using IKS.BusinessLogic.Extensions;
using IKS.Database;
using IKS.Database.Repositories;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static IKS.Database.MockDb;

namespace IKS.BusinessLogic.Managers
{
    public class UserManager : IUserManager
    {
        public IUserRepository UserRepository { get; set; }
        public UserManager(IUserRepository userRepository)
        {
            this.UserRepository = userRepository;
        }

        public UserModel GetUser(Guid id)
        {
            var entity = UserRepository.GetByID(id);
            var model = Mapper<Users, UserModel>.Map(entity);
            //model.Gender = (Genders)entity.GenderId;
            return model;
        }

        public List<UserModel> GetAllUsers()
        {
            var users = UserRepository.Get();
            return users.Select(u => Mapper<Users, UserModel>.Map(u)).ToList();
        }

        public UserModel Save(UserModel model)
        {
            if (model.UserId == Guid.Empty)
                model.UserId = Guid.NewGuid();
            var entity = Mapper<UserModel, Users>.Map(model);
            entity.GenderId = (short)model.GenderId;
            entity = UserRepository.Save(entity);
            return Mapper<Users, UserModel>.Map(entity);
        }
    }
}