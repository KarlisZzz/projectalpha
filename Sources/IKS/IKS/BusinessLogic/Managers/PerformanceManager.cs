﻿using IKS.BusinessLogic.Extensions;
using IKS.Database;
using IKS.Database.Repositories;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.BusinessLogic.Managers
{
    public class PerformanceManager : IPerformanceManager
    {
        public IPerformanceRepository performanceRepository { get; set; }
        public IEventPerformanceRepository eventPerfRepository { get; set; }
        public PerformanceManager(IPerformanceRepository PerformanceRepository, IEventPerformanceRepository eventPerfRepository)
        {
            this.performanceRepository = PerformanceRepository;
            this.eventPerfRepository = eventPerfRepository;
        }

        public PerformanceModel GetPerformance(Guid id)
        {
            return Mapper<Performances, PerformanceModel>.Map(performanceRepository.GetByID(id));
        }

        public List<PerformanceModel> GetAllPerformances()
        {
            var Performances = performanceRepository.Get();
            return Performances.Select(u => Mapper<Performances, PerformanceModel>.Map(u)).ToList();
        }
        public List<PerformanceModel> GetAllPerformancesByEventId(Guid eventId)
        {
            var Performances = performanceRepository.GetPerformancesByEventId(eventId);
            var model = Performances.Select(u => Mapper<Performances, PerformanceModel>.Map(u)).ToList();
            foreach (var item in model)
                item.EventId = eventId;
            return model;
        }

        public List<SelectPerformanceModel> GetAllSelectablePerformances(Guid eventId)
        {
            var Performances = performanceRepository.GetPerformancesNotIncludedinEventId(eventId);
            return Performances.Select(u => Mapper<Performances, SelectPerformanceModel>.Map(u)).ToList();
        }

        public PerformanceModel Save(PerformanceModel model)
        {
            if (model.PerformanceId == Guid.Empty)
                model.PerformanceId = Guid.NewGuid();
            var entity = Mapper<PerformanceModel, Performances>.Map(model);
            entity = performanceRepository.Save(entity);
            eventPerfRepository.AddPerformance(model);
            return Mapper<Performances, PerformanceModel>.Map(entity);
        }
        public void Delete(Guid id, Guid? eventId)
        {
            if (eventId.HasValue)
            {
                eventPerfRepository.Delete(id, eventId.Value);
            }
            else
            {
                performanceRepository.Delete(id);
            }
        }

    }
}