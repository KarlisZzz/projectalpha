﻿using System;
using System.Collections.Generic;
using IKS.Database.Repositories;
using IKS.Models;

namespace IKS.BusinessLogic.Managers
{
    public interface IPerformanceManager
    {
        IPerformanceRepository performanceRepository { get; set; }

        List<PerformanceModel> GetAllPerformances();
        PerformanceModel GetPerformance(Guid id);
        List<PerformanceModel> GetAllPerformancesByEventId(Guid eventId);
        List<SelectPerformanceModel> GetAllSelectablePerformances(Guid eventId);
        PerformanceModel Save(PerformanceModel model);
        void Delete(Guid id, Guid? eventId);
    }
}