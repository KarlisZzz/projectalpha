﻿using System;
using System.Collections.Generic;
using IKS.Database.Repositories;
using IKS.Models;

namespace IKS.BusinessLogic.Managers
{
    public interface IItemManager
    {
        IItemRepository itemRepository { get; set; }
        ItemModel GetItem(Guid id);
        List<ItemModel> GetAllItems();
        List<ItemModel> GetItemsByParentId(Guid parentId);
        List<ItemModel> GetItemsByUserId(Guid userId);
        ItemModel Save(ItemModel model);
    }
}