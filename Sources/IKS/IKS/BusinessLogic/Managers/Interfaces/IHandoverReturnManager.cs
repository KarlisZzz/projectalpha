﻿using IKS.Models;
using System;
using System.Collections.Generic;

namespace IKS.BusinessLogic.Managers
{
    public interface IHandoverReturnManager
    {
        void AddItems(Guid userId, string itemIds);
        void ReturnItems(Guid userId, string itemIds);
        List<ItemModel> GetAllAvailableItems();
    }
}