﻿using System;
using System.Collections.Generic;
using IKS.Models;

namespace IKS.BusinessLogic.Managers
{
    public interface IUserManager
    {
        UserModel GetUser(Guid id);
        List<UserModel> GetAllUsers();
        UserModel Save(UserModel model);
    }
}