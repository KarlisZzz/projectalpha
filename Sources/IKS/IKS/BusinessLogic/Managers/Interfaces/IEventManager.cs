﻿using System;
using System.Collections.Generic;
using IKS.Models;

namespace IKS.BusinessLogic.Managers
{
    public interface IEventManager
    {
        EventModel GetEvent(Guid id);
        List<EventModel> GetAllEvents();
        EventModel Save(EventModel model);
        void AddPerformances(Guid eventId, string performanceIds);
        List<EventModel> GetEventsByPerformanceId(Guid performanceId);
    }
}