﻿using IKS.BusinessLogic.Extensions;
using IKS.Database;
using IKS.Database.Repositories;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.BusinessLogic.Managers
{
    public class HandoverReturnManager : IHandoverReturnManager
    {
        private IUserItemRepository userItemRepository { get; set; }
        private IItemRepository itemRepository { get; set; }
        public HandoverReturnManager(IUserItemRepository userItemRepository, IItemRepository itemRepository)
        {
            this.userItemRepository = userItemRepository;
            this.itemRepository = itemRepository;
        }
        public void AddItems(Guid userId, string itemIds)
        {
            var itemSet = itemIds.Split(',');
            var itemList = itemSet.Select(i => new Guid(i.Replace("cbx_", ""))).ToList();
            userItemRepository.AddItems(userId, itemList);
        }
        public void ReturnItems(Guid userId, string itemIds)
        {
            var itemSet = itemIds.Split(',');
            var itemList = itemSet.Select(i => new Guid(i.Replace("cbx_", ""))).ToList();
            userItemRepository.ReturnItems(userId, itemList);
        }
        public List<ItemModel> GetAllAvailableItems()
        {
            var items = itemRepository.GetAllAvailableItems();
            return items.Select(u => Mapper<AvailableItems, ItemModel>.Map(u)).ToList();
        }
    }
}