﻿using IKS.BusinessLogic.Extensions;
using IKS.Database;
using IKS.Database.Repositories;
using IKS.Grids;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.BusinessLogic.Managers
{
    public class ItemManager : IItemManager
    {
        public IItemRepository itemRepository { get; set; }
        public ItemManager(IItemRepository itemRepository)
        {
            this.itemRepository = itemRepository;
        }
        public ItemModel GetItem(Guid id)
        {
            return Mapper<Items, ItemModel>.Map(itemRepository.GetByID(id));
        }


        public List<ItemModel> GetAllItems()
        {
            var items = itemRepository.Get();
            return items.Select(u => Mapper<Items, ItemModel>.Map(u)).ToList();
        }
        public List<ItemModel> GetItemsByParentId(Guid parentId)
        {
            var items = itemRepository.GetItemsByParentId(parentId);
            return items.Select(u => Mapper<Items, ItemModel>.Map(u)).ToList();
        }
        public List<ItemModel> GetItemsByUserId(Guid userId)
        {
            var items = itemRepository.GetItemsByUserId(userId);
            return items.Select(u => Mapper<Items, ItemModel>.Map(u)).ToList();
        }

        public ItemModel Save(ItemModel model)
        {
            if (model.ItemId == Guid.Empty)
                model.ItemId = Guid.NewGuid();
            var entity = Mapper<ItemModel, Items>.Map(model);
            entity = itemRepository.Save(entity);
            return Mapper<Items, ItemModel>.Map(entity);
        }


    }
}