﻿using IKS.BusinessLogic.Extensions;
using IKS.Database;
using IKS.Database.Repositories;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.BusinessLogic.Managers
{
    public class EventManager : IEventManager
    {
        public IEventRepository EventRepository { get; set; }
        public IEventPerformanceRepository EventPerformanceRepository { get; set; }
        public EventManager(IEventRepository EventRepository, IEventPerformanceRepository EventPerformanceRepository)
        {
            this.EventRepository = EventRepository;
            this.EventPerformanceRepository = EventPerformanceRepository;
        }

        public EventModel GetEvent(Guid id)
        {
            return Mapper<Events, EventModel>.Map(EventRepository.GetByID(id));
        }

        public List<EventModel> GetAllEvents()
        {
            var Events = EventRepository.Get();
            return Events.Select(u => Mapper<Events, EventModel>.Map(u)).ToList();
        }
        public EventModel Save(EventModel model)
        {
            var entity = Mapper<EventModel, Events>.Map(model);
            entity = EventRepository.Save(entity);
            return Mapper<Events, EventModel>.Map(entity);
        }
        public void AddPerformances(Guid eventId, string performanceIds)
        {
            if (!string.IsNullOrEmpty(performanceIds))
            {
                var idStrings = performanceIds.Split(',');
                List<Guid> idList = new List<Guid>();

                foreach (var item in idStrings)
                {
                    if (Guid.TryParse(item, out Guid result))
                    {
                        idList.Add(result);
                    }
                }
                if(idList.Any())
                    EventPerformanceRepository.AddPerformances(eventId, idList);
            }
        }
        public List<EventModel> GetEventsByPerformanceId(Guid performanceId)
        {
            var Events = EventRepository.GetEventsByPerformanceId(performanceId);
            return Events.Select(u => Mapper<Events, EventModel>.Map(u)).ToList();
        }
    }
}