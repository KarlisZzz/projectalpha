﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace IKS.BusinessLogic.Extensions
{
    public static class Mapper<TEntity, TDto> where TEntity : class
                                        where TDto : class, new()
    {
        public static TDto Map(TEntity entity)
        {
            var dtoObject = new TDto();
            List<PropertyInfo> dtoProperties = typeof(TDto).GetProperties().ToList();
            foreach (PropertyInfo item in typeof(TEntity).GetProperties())
            {
                var prop = dtoProperties.FirstOrDefault(d => d.Name == item.Name && d.PropertyType == item.PropertyType);
                if (prop != null)
                    prop.SetValue(dtoObject, item.GetValue(entity));
            }
            return (TDto)dtoObject;
        }
    }
}