﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IKS.Controllers
{
    public class UserController : Controller
    {
        public IUserManager userManager { get; set; }
        public UserController(IUserManager userManager)
        {
            this.userManager = userManager;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddUser()
        {
            return RedirectToAction(nameof(Edit), new { id=Guid.Empty });
        }

        [ValidateAntiForgeryToken]
        public ActionResult SaveUser(UserModel user)
        {
            if(user.IsNew)
                return RedirectToAction(nameof(Edit), new { id = Guid.Empty });
            else
                return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public ActionResult Edit(Guid? id)
        {
            UserModel model;
            if (id.HasValue)
                model = userManager.GetUser(id.Value);
            else
                model = new UserModel();

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserModel model)
        {
            if (ModelState.IsValid)
            {
                userManager.Save(model);
            }
            return View(model);
        }
    }
}