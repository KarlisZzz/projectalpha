﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IKS.Controllers
{
    public class HandoverReturnController : Controller
    {
        public IUserManager userManager { get; set; }
        public IHandoverReturnManager hrManager { get; set; }
        public IItemManager itemManager { get; set; }
        public HandoverReturnController(IUserManager userManager, IHandoverReturnManager hrManager, IItemManager itemManager)
        {
            this.userManager = userManager;
            this.hrManager = hrManager;
            this.itemManager = itemManager;
        }
        public ActionResult Index(Guid? id = null)
        {
            UserModel userModel = null;
            if(id.HasValue)
                userManager.GetUser(id.Value);
            else
                userModel = userManager.GetAllUsers().FirstOrDefault();

            var model = new HandoverModel() { User = userModel };
            return View(model);
        }
        public JsonResult Handover(Guid userId, string itemIds)
        {
            hrManager.AddItems(userId, itemIds);
            return Json("");
        }
        public JsonResult Return(Guid userId, string itemIds)
        {
            hrManager.ReturnItems(userId, itemIds);
            return Json("");
        }
        public ActionResult CardSelect(Guid? id = null)
        {
            var user = userManager.GetAllUsers().FirstOrDefault();
            var model = itemManager.GetItemsByUserId(user.UserId);
            return View(model);
        }
        public PartialViewResult SelectItem(ItemModel model)
        {
            return PartialView(model);
        }
    }
}