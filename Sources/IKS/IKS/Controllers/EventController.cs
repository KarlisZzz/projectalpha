﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IKS.Controllers
{
    public class EventController : Controller
    {
        public IEventManager eventManager { get; set; }
        public EventController(IEventManager eventManager)
        {
            this.eventManager = eventManager;
        }
        // GET: Concert
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Edit(Guid? id)
        {
            EventModel model;
            if (id.HasValue)
                model = eventManager.GetEvent(id.Value);
            else
                model = new EventModel();

            return View(model);
        }
        [HttpPost]
        public ActionResult Edit( EventModel model)
        {
            if (ModelState.IsValid)
            {
                eventManager.Save(model);
            }
            return View(model);
        }
        public ActionResult PerformanceSelect(Guid eventId)
        {
            var model = new MainSelectPerformanceViewModel()
            {
                EventId = eventId
            };
            return PartialView(model);
        }
        public void AddPerformances(Guid eventId, string performanceIds)
        {
            eventManager.AddPerformances(eventId, performanceIds);
        }
    }
}