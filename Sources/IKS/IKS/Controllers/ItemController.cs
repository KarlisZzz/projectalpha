﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IKS.Controllers
{
    public class ItemController : Controller
    {
        public IItemManager itemManager { get; set; }
        public ItemController(IItemManager itemManager)
        {
            this.itemManager = itemManager;
        }
        public ActionResult Index()
        {
            ViewBag.ColumnVisibility = JsonConvert.SerializeObject(GetColumnVisibility().Data);
            return View();
        }
        [HttpGet]
        public ActionResult Edit(Guid? id, Guid? parentId)
        {
            ItemModel model;
            if (id.HasValue)
                model = itemManager.GetItem(id.Value);
            else
                model = new ItemModel();

            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(ItemModel model)
        {
            if (ModelState.IsValid)
            {
                itemManager.Save(model);
            }
            return View(model);
        }
        public ActionResult AddItem()
        {
            return RedirectToAction(nameof(Edit), new { id = Guid.Empty });
        }

        public JsonResult GetColumnVisibility()
        {
            
            var item = new
            {
                Code = true,
                Name = true,
                Description = false
            };
            return Json(item);
        }
    }
}