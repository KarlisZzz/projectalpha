﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IKS.Controllers
{
    public class PerformanceController : Controller
    {
        public IPerformanceManager perfManager { get; set; }
        public PerformanceController(IPerformanceManager perfManager)
        {
            this.perfManager = perfManager;
        }
        // GET: Performance
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Edit(Guid? id, Guid? eventId)
        {
            PerformanceModel model;
            if (id.HasValue)
                model = perfManager.GetPerformance(id.Value);
            else
                model = new PerformanceModel(eventId);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PerformanceModel model)
        {
            if (ModelState.IsValid)
            {
                perfManager.Save(model);
                //return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        public void Delete(Guid id, Guid? eventId)
        {
            if (ModelState.IsValid)
            {
                perfManager.Delete(id, eventId);
                //return RedirectToAction("Index");
            }
        }
    }
}