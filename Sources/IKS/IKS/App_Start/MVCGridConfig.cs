[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(IKS.MVCGridConfig), "RegisterGrids")]

namespace IKS
{
    using IKS.Grids;
    using IKS.Models;
    using MVCGrid.Models;
    using MVCGrid.Web;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Unity;

    public static class MVCGridConfig 
    {
        public static void RegisterGrids()
        {
            MVCGridDefinitionTable.Add(nameof(UserGrid), GridResolver<UserGrid>.Resolve().Init);
            MVCGridDefinitionTable.Add(nameof(ItemGrid), GridResolver<ItemGrid>.Resolve().Init);
            MVCGridDefinitionTable.Add(nameof(ItemHandoverGrid), GridResolver<ItemHandoverGrid>.Resolve().Init);
            MVCGridDefinitionTable.Add(nameof(EventGrid), GridResolver<EventGrid>.Resolve().Init);
            MVCGridDefinitionTable.Add(nameof(PerformanceGrid), GridResolver<PerformanceGrid>.Resolve().Init);
            MVCGridDefinitionTable.Add(nameof(SelectPerformanceGrid), GridResolver<SelectPerformanceGrid>.Resolve().Init);
        }
    }
    internal static class GridResolver<T> where T:class
    {
        internal static T Resolve()
        {
            return (T)UnityConfig.Container.Resolve(typeof(T));
        }
    }

}