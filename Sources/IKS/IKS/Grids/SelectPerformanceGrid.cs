﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using MVCGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace IKS.Grids
{
    public class SelectPerformanceGrid : BaseGrid<SelectPerformanceModel>
    {
        public IPerformanceManager perfManager { get; set; }
        public SelectPerformanceGrid(IPerformanceManager perfManager)
        {
            this.perfManager = perfManager;
        }
        public override MVCGridBuilder<SelectPerformanceModel> Init => new MVCGridBuilder<SelectPerformanceModel>(defaultSet, colDefaults)
                .WithAuthorizationType(AuthorizationType.AllowAnonymous)
                .AddColumns(cols =>
                {
                    cols.Add("Select").WithHtmlEncoding(false)
                        .WithSorting(false)
                        .WithHeaderText(" ")
                        .WithValueExpression((p, c) => p.PerformanceId.ToString())
                        .WithValueTemplate("<input type='checkbox' id='cbx_{Value}' onchange='CheckboxChanged(this)' style='display: none'><label for='cbx_{Value}' class='toggle'><span></span></label>");
                    cols.Add(ReferenceColumn(nameof(SelectPerformanceModel.PerformanceName), (p, c) => c.UrlHelper.Action("Edit", "Performances", new { id = p.PerformanceId }), "Name"));
                })
                .WithSorting(true, nameof(SelectPerformanceModel.PerformanceName))
                .WithFiltering(true)
                .WithPageParameterNames(new[] { "eventId" })
                .WithRetrieveDataMethod((context) =>
                {
                    Options = context.QueryOptions;
                    string eventId = Options.GetPageParameterString("eventId");
                    List<SelectPerformanceModel> itemList;
                    itemList = perfManager.GetAllSelectablePerformances(Guid.Parse(eventId));
                    var result = new QueryResult<SelectPerformanceModel>();

                    IEnumerable<SelectPerformanceModel> items = itemList.OrderBy(p => p.PerformanceName);

                    return ReturnQueryResult(ref items);

                });

    }
}