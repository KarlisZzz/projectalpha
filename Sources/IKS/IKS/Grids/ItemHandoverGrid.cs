﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using MVCGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Grids
{
    public class ItemHandoverGrid : BaseGrid<ItemModel>
    {
        public IItemManager itemManager { get; set; }
        public IHandoverReturnManager hrManager { get; set; }
        public ItemHandoverGrid(IItemManager itemManager, IHandoverReturnManager hrManager)
        {
            this.itemManager = itemManager;
            this.hrManager = hrManager;
        }
        public const string UserId = "UserId";
        public override MVCGridBuilder<ItemModel> Init => new MVCGridBuilder<ItemModel>(defaultSet, colDefaults)
                .WithAuthorizationType(AuthorizationType.AllowAnonymous)
                .AddColumns(cols =>
                {
                    cols.Add(CheckboxColumn((p, c) => p.ItemId.ToString()));
                    cols.Add(BasicColumn(nameof(ItemModel.Name)));
                    cols.Add(BasicColumn(nameof(ItemModel.Code)));
                })
                .WithSorting(true, nameof(ItemModel.Name))
                .WithPageParameterNames(new[] { nameof(GridType), UserId })
                .WithRetrieveDataMethod((context) =>
                {
                    Options = context.QueryOptions;

                    string gridType = Options.GetPageParameterString(nameof(GridType));
                    string userId = Options.GetPageParameterString(UserId);
                    List<ItemModel> itemList;
                    if (string.IsNullOrEmpty(userId))
                        itemList = hrManager.GetAllAvailableItems();
                    else
                        itemList = itemManager.GetItemsByUserId(Guid.Parse(userId));
                    IEnumerable<ItemModel> items = itemList.OrderBy(p => p.Name);
                    return ReturnQueryResult(ref items);

                });
    }
    public enum GridType
    {
        None, Recieve, Hadover
    }
}