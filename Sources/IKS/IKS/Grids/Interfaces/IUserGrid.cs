﻿using IKS.BusinessLogic.Managers;
using IKS.Models;
using MVCGrid.Models;

namespace IKS.Grids
{
    public interface IUserGrid
    {
        MVCGridBuilder<UserModel> Init { get; }
        IUserManager userManager { get; set; }
    }
}