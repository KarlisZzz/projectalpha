﻿using IKS.Models;
using MVCGrid.Models;

namespace IKS.Grids
{
    public interface IGrid<TEntity> where TEntity : class
    {
        MVCGridBuilder<TEntity> Init { get; }
    }
}