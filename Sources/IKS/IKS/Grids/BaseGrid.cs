﻿using MVCGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

namespace IKS.Grids
{
    public abstract class BaseGrid<T> : IGrid<T> where T: class
    {
        public virtual MVCGridBuilder<T> Init => new MVCGridBuilder<T>();
        public QueryOptions Options { get; set; }

        private const string referenceValueTemplate = @"<a href='{Value}' role='button'>{Model.{0}}</a>";
        public GridDefaults defaultSet = new GridDefaults()
        {
            Paging = true,
            ItemsPerPage = 20,
            Sorting = true,
            NoResultsMessage = "Sorry, no results were found"
        };

        public ColumnDefaults colDefaults = new ColumnDefaults()
        {
            EnableSorting = true,
            EnableFiltering = true
        };

        protected IEnumerable<T> SetSortingAndFilters(QueryOptions options, IEnumerable<T> items)
        {
            foreach (PropertyInfo info in typeof(T).GetProperties())
            {
                var filter = options?.GetFilterString(info.Name);
                if (!string.IsNullOrEmpty(filter) && filter != "null")
                {
                    items = items.Where(u => info.GetValue(u, null).ToString().Contains(filter));
                }
            }
            if (!string.IsNullOrEmpty(options?.SortColumnName))
            {
                PropertyInfo sortPropInfo = typeof(T).GetProperty(options.SortColumnName);
                items = options.SortDirection == SortDirection.Asc ? items.OrderBy(p => sortPropInfo.GetValue(p, null)) : items.OrderByDescending(p => sortPropInfo.GetValue(p, null));
            }

            return items;
        }

        protected GridColumn<T> BasicColumn(string columnName, string display = "")
        {
            if (string.IsNullOrEmpty(display))
                display = columnName;
            var gcb = new GridColumnBuilder<T>()
                        .WithColumnName(columnName)
                        .WithHeaderText(display)
                        .WithFiltering(true)
                        .WithValueExpression(i => i.GetType().GetProperty(columnName).GetValue(i)?.ToString());
            return gcb.GridColumn;
        }

        protected GridColumn<T> ReferenceColumn(string columnName, Func<T,GridContext, string> valueExpression ,string display = "")
        {
            if (string.IsNullOrEmpty(display))
                display = columnName;
            var gcb = new GridColumnBuilder<T>()
                        .WithColumnName(columnName)
                        .WithHeaderText(display)
                        .WithValueExpression(valueExpression)
                        .WithValueTemplate("<a href='{Value}' role='button'>{Model."+ columnName + "}</a>", false)
                        .WithPlainTextValueExpression(i => i.GetType().GetProperty(columnName).GetValue(i).ToString()); 
            return gcb.GridColumn;
        }
        protected GridColumn<T> CheckboxColumn(Func<T, GridContext, string> valueExpression)
        {
            var gcb = new GridColumnBuilder<T>()
                        .WithColumnName("Select")
                        .WithHtmlEncoding(false)
                        .WithSorting(false)
                        .WithHeaderText(" ")
                        .WithValueExpression(valueExpression)
                        .WithValueTemplate("<input type='checkbox' id='cbx_{Value}' style='display: none'><label for='cbx_{Value}' class='toggle'><span></span></label>");
            return gcb.GridColumn;
        }


        protected QueryResult<T> ReturnQueryResult(ref IEnumerable<T> items)
        {
            items = SetSortingAndFilters(Options, items);
            var count = items.Count();
            if (Options.GetLimitOffset().HasValue)
            {
                items = items.Skip(Options.GetLimitOffset().Value).Take(Options.GetLimitRowcount().Value);
            }
            return new QueryResult<T>()
            {
                Items = items,
                TotalRecords = count
            };
        }
    }
}