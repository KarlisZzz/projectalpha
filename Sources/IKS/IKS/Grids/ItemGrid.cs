﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using MVCGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace IKS.Grids
{
    public class ItemGrid : BaseGrid<ItemModel>
    {
        public IItemManager itemManager { get; set; }
        public ItemGrid(IItemManager itemManager)
        {
            this.itemManager = itemManager;
        }
        public override MVCGridBuilder<ItemModel> Init => new MVCGridBuilder<ItemModel>(defaultSet, colDefaults)
                .WithAuthorizationType((AuthorizationType)AuthorizationType.AllowAnonymous)
                .AddColumns(cols =>
                {
                    cols.Add(ReferenceColumn(nameof(ItemModel.Name), (p, c) => (string)c.UrlHelper.Action("Edit", "Item", new { id = p.ItemId }), "Name"));
                    cols.Add(BasicColumn(nameof(ItemModel.Code)));
                    cols.Add(BasicColumn(nameof(ItemModel.Description)));
                    //cols.Add((string)"Delete").WithHtmlEncoding((bool)false)
                    //    .WithSorting((bool)false)
                    //    .WithHeaderText((string)" ")
                    //    .WithVisibility((bool)false, (bool)true)
                    //    .WithValueExpression((p, c) => c.UrlHelper.Action("Delete", "Items", new { id = p.ItemId }))
                    //    //.WithValueTemplate("<a href='{Value}' class='btn btn-danger' role='button'>Delete</a>");
                    //    .WithValueTemplate((string)"<button class='btn btn-danger' onclick='DeletePerformance(\"{Value}\")'>Delete</button>");
                })
                .WithSorting(true, nameof(ItemModel.Name))
                .WithFiltering(true)
                .WithPageParameterNames(new[] { "parentId" })
                .WithRetrieveDataMethod((context) =>
                {
                    Options = context.QueryOptions;

                    string parentId = Options.GetPageParameterString("parentId");
                    List<ItemModel> itemList;
                    if (string.IsNullOrEmpty(parentId))
                        itemList = itemManager.GetAllItems();
                    else
                        itemList = itemManager.GetItemsByParentId(Guid.Parse(parentId));
                    IEnumerable<ItemModel> items = itemList.OrderBy(p => p.Name);
                    return ReturnQueryResult(ref items);

                });

    }
}