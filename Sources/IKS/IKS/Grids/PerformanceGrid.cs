﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using MVCGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace IKS.Grids
{
    public class PerformanceGrid : BaseGrid<PerformanceModel>
    {
        public IPerformanceManager perfManager { get; set; }
        public PerformanceGrid(IPerformanceManager perfManager)
        {
            this.perfManager = perfManager;
        }
        public override MVCGridBuilder<PerformanceModel> Init => new MVCGridBuilder<PerformanceModel>(defaultSet, colDefaults)
                .WithAuthorizationType(AuthorizationType.AllowAnonymous)
                .AddColumns(cols =>
                {
                    cols.Add(ReferenceColumn(nameof(PerformanceModel.PerformanceName), (p, c) => c.UrlHelper.Action("Edit", "Performance", new { id = p.PerformanceId }), "Name"));
                    cols.Add(BasicColumn(nameof(PerformanceModel.Description)));
                    //cols.Add(BasicColumn(nameof(PerformanceModel.Status)));
                    cols.Add("Delete").WithHtmlEncoding(false)
                        .WithSorting(false)
                        .WithHeaderText(" ")
                        .WithValueExpression((p, c) => c.UrlHelper.Action("Delete", "Performance", new { id = p.PerformanceId, eventId = p.EventId }))
                        //.WithValueTemplate("<a href='{Value}' class='btn btn-danger' role='button'>Delete</a>");
                        .WithValueTemplate("<button class='btn btn-danger' onclick='DeletePerformance(\"{Value}\")'>Delete</button>");
                })
                .WithSorting(true, nameof(PerformanceModel.PerformanceName))
                .WithFiltering(true)
                .WithPageParameterNames(new[] { "eventId" })
                .WithRetrieveDataMethod((context) =>
                {
                    Options = context.QueryOptions;
                    string eventId = Options.GetPageParameterString("eventId");
                    List<PerformanceModel> itemList;
                    if (string.IsNullOrEmpty(eventId))
                        itemList = perfManager.GetAllPerformances();
                    else
                        itemList = perfManager.GetAllPerformancesByEventId(Guid.Parse(eventId));

                    var result = new QueryResult<PerformanceModel>();

                    IEnumerable<PerformanceModel> items = itemList.OrderBy(p => p.PerformanceName);

                    return ReturnQueryResult(ref items);

                });

    }
}