﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using MVCGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace IKS.Grids
{
    public class EventGrid : BaseGrid<EventModel>
    {
        public IEventManager eventManager { get; set; }
        public EventGrid(IEventManager eventManager)
        {
            this.eventManager = eventManager;
        }
        public override MVCGridBuilder<EventModel> Init => new MVCGridBuilder<EventModel>(defaultSet, colDefaults)
                .WithAuthorizationType(AuthorizationType.AllowAnonymous)
                .AddColumns(cols =>
                {
                    
                    cols.Add(ReferenceColumn(nameof(EventModel.EventName), (p, c) => c.UrlHelper.Action("Edit", "Event", new { id = p.EventId }), "Name"));
                    cols.Add(BasicColumn(nameof(EventModel.EventDescription)));
                    //cols.Add().WithColumnName("EventDate")
                    //    .WithHeaderText("Date")
                    //    //.WithCellCssClassExpression(p => p.EventDate > DateTime.Today ? "danger" : "warning")
                    //    .WithCellCssClassExpression(p => "danger")
                    //    .WithValueExpression(i => i.Status.ToString());
                })
                .WithSorting(true, nameof(EventModel.EventName))
                .WithRetrieveDataMethod((context) =>
                {
                    //ar itemList = MockDb.GetAllConcerts();
                    var itemList = eventManager.GetAllEvents();
                    Options = context.QueryOptions;
                    var result = new QueryResult<EventModel>();

                    IEnumerable<EventModel> items = itemList.OrderBy(p => p.EventName);
                    return ReturnQueryResult(ref items);
                });

    }
}