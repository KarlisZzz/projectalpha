﻿using IKS.BusinessLogic.Managers;
using IKS.Database;
using IKS.Models;
using MVCGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace IKS.Grids
{
    public class UserGrid : BaseGrid<UserModel>, IUserGrid
    {
        public IUserManager userManager { get; set; }
        public UserGrid(IUserManager userManager)
        {
            this.userManager = userManager;
        }
        public override MVCGridBuilder<UserModel> Init => new MVCGridBuilder<UserModel>(defaultSet, colDefaults)
                .WithAuthorizationType(AuthorizationType.AllowAnonymous)
                .AddColumns(cols =>
                {
                    cols.Add(ReferenceColumn(nameof(UserModel.Login), (p, c) => c.UrlHelper.Action("Edit", "User", new { id = p.UserId }), "Login"));
                    DefaultUserCols(cols);
                })
                .WithSorting(true, nameof(UserModel.UserName))
                .WithFiltering(true)
                .WithRetrieveDataMethod((context) =>
                {
                    Options = context.QueryOptions;

                    var itemList = userManager.GetAllUsers();
                    var result = new QueryResult<UserModel>();

                    IEnumerable<UserModel> items = itemList.OrderBy(p => p.Login);

                    return ReturnQueryResult(ref items);

                });

        private void DefaultUserCols(GridColumnListBuilder<UserModel> cols)
        {
            cols.Add(BasicColumn(nameof(UserModel.UserName)));
            cols.Add(BasicColumn(nameof(UserModel.Surname)));
            cols.Add(BasicColumn(nameof(UserModel.GenderId)));
        }
    }
}