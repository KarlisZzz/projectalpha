﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Models
{
    public class EventModel
    {
        public Guid EventId { get; set; }
        public string EventName  { get; set; }
        public string EventDescription { get; set; }
        public DateTime EventDate { get; set; }
        public ItemStatus Status { get; internal set; }
    }
}