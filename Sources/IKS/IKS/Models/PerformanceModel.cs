﻿using IKS.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Models
{
    public class PerformanceModel
    {
        public PerformanceModel()
        {
        }
        public PerformanceModel(Guid? EventId)
        {
            PerformanceId = Guid.NewGuid();
            if(EventId.HasValue)
                this.EventId = EventId.Value;
        }
        public Guid PerformanceId { get; set; }
        public Guid? EventId { get; set; }
        public string PerformanceName{ get; set; }
        public string Description { get; set; }
        public ItemStatus Status { get; internal set; }
    }
}