﻿using IKS.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Models
{
    public class SelectPerformanceModel
    {
        public Guid PerformanceId { get; set; }
        public string PerformanceName{ get; set; }
    }
    public class MainSelectPerformanceViewModel
    {
        public Guid EventId { get; set; }
    }
}