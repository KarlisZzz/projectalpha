﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static IKS.Database.MockDb;

namespace IKS.Models
{
    public class UserModel
    {
        public Guid UserId { get; set; }
        public string Login { get; set; }
        public string UserName { get; set; }
        public string Surname { get; set; }
        public Genders GenderId { get; set; }
        public UserStatus Status { get; set; }
        public bool IsNew { get; set; }

    }
    public enum UserStatus
    {
        Active = 1,
        Deactivated = 2
    }
}