﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IKS.Models
{
    public class ItemModel : BaseModel
    {
        public ItemModel()
        {
        }
        public ItemModel(Guid? parentId = null)
        {
            ItemId = Guid.NewGuid();
            if (parentId.HasValue)
                ParentId = parentId;
        }
        public Guid ItemId { get; set; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public ItemStatus Status { get; set; }
        public bool IsNew { get; set; }
        public bool Parent { get; set; }


    }

    public enum ItemStatus
    {
        Active = 1,
        Deactivated = 2
    }
}