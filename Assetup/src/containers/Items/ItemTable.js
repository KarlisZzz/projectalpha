import React, { Component } from "react";
import TableWrapper, {
  CustomizedTableWrapper
} from "../Tables/antTables/antTable.style";
import Switch from "../../components/uielements/switch";
import Form from "../../components/uielements/form";
// import { AntTable } from "./index";
import fakeData from "./fakeData";
// import LayoutContentWrapper from "../../components/utility/layoutWrapper.js";
// import { columns } from "./configs";
// import clone from "clone";
const FormItem = Form.Item;
const dataList = new fakeData(10);
const expandedRowRender = record => (
  <p>{`${record.firstName} lives in ${record.city}`}</p>
);
const title = () => "Here is title";
const showHeader = true;
const footer = () => "Here is footer";
export default class extends Component {
  constructor(props) {
    super(props);
    this.renderSwitch = this.renderSwitch.bind(this);
    this.state = {
      bordered: undefined,
      loading: undefined,
      pagination: true,
      size: "default",
      expandedRowRender,
      title,
      showHeader,
      footer,
      rowSelection: {},
      scroll: undefined
    };
  }
  renderSwitch(option) {
    const checked = this.state[option.key] !== undefined;
    const onChange = () => {
      if (!checked) {
        this.setState({ [option.key]: option.defaultValue });
      } else {
        this.setState({ [option.key]: undefined });
      }
    };
    return (
      <FormItem label={option.title} key={option.key}>
        <Switch checked={checked} onChange={onChange} />
      </FormItem>
    );
  }
  render() {
    return (
      <CustomizedTableWrapper className="isoCustomizedTableWrapper">
        <TableWrapper
          {...this.state}
          columns={this.props.tableInfo.columns}
          dataSource={this.props.dataList.getAll()}
          className="isoCustomizedTable"
        />
      </CustomizedTableWrapper>
    );
  }
}
