import React, { Component } from "react";
import LayoutContentWrapper from "../../components/utility/layoutWrapper.js";
import fakeData from "./fakeData";
import { columns } from "./configs";
import clone from "clone";
import ItemTable from "./ItemTable";

const dataList = new fakeData(10);

export default class AntTable extends Component {
  renderTable(dataList) {
    let Component;
    let tableInfo = {
      title: "Item Table",
      value: "ItemTable",
      columns: clone(columns)
    };
    Component = ItemTable;
    return <Component tableInfo={tableInfo} dataList={dataList} />;
  }
  render() {
    return (
      <LayoutContentWrapper>{this.renderTable(dataList)}</LayoutContentWrapper>
    );
  }
}
export { dataList, AntTable };
