const tableData = JSON.parse(
  `[
    {
      "itemId":"5d45836c548a20ebe6f2882a",
      "code":"DESERUNT",
      "description":"Fugiat laborum sit magna consectetur do nisi do. Mollit amet et ea est ipsum laboris do. Lorem Lorem consequat dolor nostrud nulla ipsum. Fugiat ullamco qui exercitation cupidatat mollit ad in officia commodo esse ad ex aute aute. Ex aute in minim aliquip aliquip Lorem.",
      "status":"AtUser",
      "given_date":"2014-10-28T09:58:39 -02:00",
      "return_date": "2020-05-08T03:09:25 -03:00",
      "gender":"Female"
    },
    {
      "itemId":"5d45836c26e74b1c603db083",
      "code":"EIUSMOD",
      "description":"Voluptate deserunt voluptate adipisicing dolor et Lorem anim Lorem deserunt minim. Nisi in quis non anim. Culpa ea nisi laborum eiusmod non est nostrud eiusmod. Commodo irure quis occaecat do duis ipsum officia. Consectetur et duis exercitation laborum. Et ipsum nostrud aliqua reprehenderit pariatur fugiat ad consequat ut mollit.",
      "status":"AtUser",
      "given_date":"2014-01-19T12:27:23 -02:00",
      "return_date": "2020-05-08T03:09:25 -03:00",
      "gender":"Female"
    },
    {
      "itemId":"5d45836cd84621e1448d33af",
      "code":"INCIDIDUNT",
      "description":"Pariatur qui consectetur voluptate tempor laboris tempor nisi irure ea in labore. Laborum amet et duis ad aute sit occaecat ea nostrud. Ad esse excepteur veniam excepteur. Cupidatat aute proident occaecat deserunt sit non velit deserunt et culpa mollit incididunt dolore. Consequat officia velit veniam deserunt. Exercitation voluptate aliqua duis cupidatat nulla officia non tempor minim tempor et. Sit dolor consequat anim enim proident.",
      "status":"Available",
      "given_date":"2014-02-26T07:26:36 -02:00",
      "return_date": "2020-05-08T03:09:25 -03:00",
      "gender":"Male"
    },
    {
      "itemId":"5d45836c7cce158af1e193d9",
      "code":"MAGNA",
      "description":"Elit nisi sint voluptate dolor non amet. Minim nostrud est Lorem velit mollit esse ad qui. Dolore ullamco voluptate dolore ipsum veniam enim esse qui proident duis velit occaecat. Voluptate officia cillum dolore do laborum eu amet ad pariatur aute minim. Ea qui quis eu anim eu aliqua amet enim sit enim laborum sunt non non. Incididunt labore velit minim qui sunt deserunt quis et sunt ex. Aliqua nulla esse Lorem in.",
      "status":"AtUser",
      "given_date":"2018-06-21T06:02:20 -03:00",
      "return_date":"date(new Date(), new Date(2020,12,12), 'YYYY-MM-ddThh:mm:ss Z')",
      "gender":"Unisex"
    },
    {
      "itemId":"5d45836c443ac67eb87aed6e",
      "code":"QUIS",
      "description":"Aliqua ipsum sit ex irure amet. Consectetur magna ipsum sit tempor consectetur ipsum incididunt in nisi dolor consectetur. Officia non ad aliqua ea incididunt et non proident sit exercitation. Veniam sit nulla cupidatat esse ullamco consectetur. Sint excepteur esse reprehenderit cupidatat.",
      "status":"AtUser",
      "given_date":"2018-02-09T02:53:25 -02:00",
      "return_date": "2020-05-08T03:09:25 -03:00",
      "gender":"Unisex"
    },
    {
      "itemId":"5d45836c342fdacc1036cd87",
      "code":"AMET",
      "description":"Dolor non incididunt velit quis ut adipisicing ea sint ut in. Dolor culpa cupidatat laborum laborum nostrud irure. Incididunt qui pariatur ullamco labore tempor amet proident sint aute elit incididunt exercitation. Do reprehenderit enim aute velit excepteur dolore irure est minim Lorem eiusmod. Id commodo dolore dolore in velit Lorem est velit cillum sunt ad laborum.",
      "status":"Broken",
      "given_date":"2017-03-01T09:01:57 -02:00",
      "return_date": "2020-05-08T03:09:25 -03:00",
      "gender":"Female"
    },
    {
      "itemId":"5d45836c6ce729d5b24f42a7",
      "code":"LOREM",
      "description":"Nisi incididunt cillum magna deserunt quis anim dolore laboris dolore ut dolor voluptate minim. Sunt quis velit esse eu non culpa ut ad minim ut excepteur esse. Nostrud labore do qui laborum nostrud elit. Fugiat voluptate enim quis ut quis dolor reprehenderit labore.",
      "status":"Broken",
      "given_date":"2019-01-01T12:17:48 -02:00",
      "return_date": "2020-05-08T03:09:25 -03:00",
      "gender":"Female"
    }
  ]`
);
const sortOption = {};
class itemFakeData {
  constructor(size) {
    this.size = size || 2000;
    this.datas = [];
    this.sortKey = null;
    this.sortDir = null;
  }
  dataModel(index) {
    return tableData[index];
  }
  getObjectAt(index) {
    if (index < 0 || index > this.size) {
      return undefined;
    }
    if (this.datas[index] === undefined) {
      this.datas[index] = this.dataModel(index);
    }
    return this.datas[index];
  }
  getAll() {
    if (this.datas.length < this.size) {
      for (let i = 0; i < this.size; i++) {
        this.getObjectAt(i);
      }
    }
    return this.datas.slice();
  }

  getSize() {
    return this.size;
  }
  getSortAsc(sortKey) {
    sortOption.sortKey = sortKey;
    sortOption.sortDir = "ASC";
    return this.datas.sort(this.sort);
  }
  getSortDesc(sortKey) {
    sortOption.sortKey = sortKey;
    sortOption.sortDir = "DESC";
    return this.datas.sort(this.sort);
  }
  sort(optionA, optionB) {
    const valueA = optionA[sortOption.sortKey].toUpperCase();
    const valueB = optionB[sortOption.sortKey].toUpperCase();
    let sortVal = 0;
    if (valueA > valueB) {
      sortVal = 1;
    }
    if (valueA < valueB) {
      sortVal = -1;
    }
    if (sortVal !== 0 && sortOption.sortDir === "DESC") {
      return sortVal * -1;
    }
    return sortVal;
  }
}
export default itemFakeData;
