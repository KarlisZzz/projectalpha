import React, { Component } from "react";
import Box from "../../components/utility/box";
import { ViewTable } from "../../components/item/itemTable";
//import Button from "../../components/uielements/button";
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import InvoicePageWrapper from "../Invoice/singleInvoice.style";
import ItemFakeData from "../Items/itemFakeData";

const dataList = new ItemFakeData(10);

export default class extends Component {
  render() {
    const { currentInvoice, toggleView, redirectPath } = this.props;
    return (
      <LayoutWrapper>
        <Box>
          <InvoicePageWrapper className="InvoicePageWrapper">
            <div className="PageContent">
              <div className="OrderInfo">
                <h2>Kārlis Ziemulis</h2>
                {/* <Link to={redirectPath}>
                <Button className="isoGoInvoBtn">
                  <span>Go To Invoices</span>
                </Button>
              </Link>
              <Button color="secondary" onClick={() => toggleView(true)}>
                <span>Edit Invoice</span>
              </Button>
              <Button type="primary" className="isoInvoPrint">
                <span>Print Invoice</span>
              </Button> */}
              </div>
              <div className="PageContent">
                <div style={{ margin: "10px 5px 15px 20px" }}>
                  <p>Items in possesion</p>
                </div>
              </div>
              <div className="InvoiceTable">
                <ViewTable dataList={dataList.getAll()} />
                {/* <div className="TotalBill">
                  <p>
                    Sub-total :{" "}
                    <span>{`${currentInvoice.currency}${
                      currentInvoice.subTotal
                    }`}</span>
                  </p>
                  <p>
                    Vat :{" "}
                    <span>{`${currentInvoice.currency}${
                      currentInvoice.vatPrice
                    }`}</span>
                  </p>
                  <h3>
                    Grand Total :{" "}
                    <span>{`${currentInvoice.currency}${
                      currentInvoice.totalCost
                    }`}</span>
                  </h3>
                </div> */}
              </div>
              {/* <div className="ButtonWrapper">
                <Button type="primary" className="mateInvoPrint">
                  <span>Send Invoice</span>
                </Button>
              </div> */}
            </div>
          </InvoicePageWrapper>
        </Box>
      </LayoutWrapper>
    );
  }
}
